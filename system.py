import platform
import sys
import cpuinfo
import psutil

def get_system_environment():
    # 获取操作系统信息
    os_name = platform.system()
    os_version = platform.release()

    # 获取Python版本信息
    python_version = sys.version

    # 获取CPU信息
    cpu_info = cpuinfo.get_cpu_info()
    cpu_architecture = cpu_info.get('arch')

    # 获取内存信息
    total_memory = psutil.virtual_memory().total

    # 打印系统环境信息
    print("操作系统：", os_name)
    print("操作系统版本：", os_version)
    print("Python版本：", python_version)
    print("CPU架构：", cpu_architecture)
    print("内存总量：", total_memory, "bytes")

if __name__ == "__main__":
    get_system_environment()

